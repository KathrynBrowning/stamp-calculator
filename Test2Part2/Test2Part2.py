# Program Name:         Test2Part2.py
# Course:               IT1113/Section W01
# Student Name:         Kathryn Browning
# Assignment Number:    Test2Part2
# Due Date:             03/18/2018

# This program calculates and displays how many total stamps have been 
# collected for a year, after user enters in the month and the number 
# of stamps collected for each month.
def main():
    month_num = 0
    stamps = 0

    print('\nWelcome to the stamp collector program! After entering the stamps collected for each month,' 
        + ' this program will show you the total amount of stamps you\'ve collected for this year!');
    
    while True:           
        month_num += 1

        month = input('\nPlease enter the name of month %d: ' %month_num)      
        stamps += int(input('\nPlease enter the number of stamps collected for %s: ' %month))

        if month_num == 12:
            print('The total number of stamps collected this year is: %d' %stamps)
            break;

main()